
const usernameFiled = '#txtUsername';
const passwordFiled = '#txtPassword';
const buttonField = '#btnLogin';

export const loginPage = {

    username(value){
        cy.get(usernameFiled)
        .type(value)
        .should('have.value', value)
    },

    password(value){
        cy.get(passwordFiled)
        .type(value)
        .should('have.value', value)
    },

    login(){
        cy.get(buttonField)
        .click()
    },

    loginApp(value_username, value_password){
        this.username(value_username)
        this.password(value_password)
        this.login

    }
}