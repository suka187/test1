import { loginPage } from '../tests/Pages/LoginPage'

describe('Login Test', function() {

    beforeEach(function() {
        cy.visit('https://opensource-demo.orangehrmlive.com/')
      })

      it.only('When user enter invalid login credentials error message is shown', function(){
          //verify you are on log in page
        cy.get('#logInPanelHeading').should('have.text', 'LOGIN Panel');

        //submit wrong credentials should gives error
        loginPage.username('fakQQQ@email.com')
        loginPage.password('123456');
        loginPage.login()

        loginPage.loginApp('suka@gmail.com','874521')

        cy.get('#spanMessage').should('have.text', 'Invalid credentials');

      })

      it('When user submit empty login credentials error message is shown', function(){

        //submit empty login form gives an user is empty error
          cy.get('#btnLogin').click()
          cy.get('#spanMessage').should('have.text', 'Username cannot be empty');

        //submit empty password fill will give password missing error
          cy.get('#txtUsername')
          .type('fake@email.com')
          .should('have.value', 'fake@email.com')

          cy.get('#btnLogin').click()
          cy.get('#spanMessage').should('have.text', 'Password cannot be empty');
      })

      it('When user submit valid username and password login is successful ', function(){

        cy.get('#txtUsername')
          .type(Cypress.env("username"))
          .should('have.value', Cypress.env("username"))

        cy.get('#txtPassword')
          .type(Cypress.env("password"))
          .should('have.value', Cypress.env("password"))

          cy.get('#btnLogin').click()

          //verify Dashboard is visible 
          cy.get('h1').should('have.text', 'Dashboard');
          
          //verify navigation is visible 
          cy.get('#menu_admin_viewAdminModule').should('be.visible')
          cy.get('#menu_pim_viewPimModule ').should('be.visible')
          cy.get('#menu_leave_viewLeaveModule').should('be.visible')
          cy.get('#menu_time_viewTimeModule').should('be.visible')
          cy.get('#menu_recruitment_viewRecruitmentModule').should('be.visible')

        })

        context('Add new system user', function(){

          beforeEach(function() {
            cy.visit(Cypress.env("url"))
            })

          it('When user is logged in successfully', function(){

          cy.get('#txtUsername')
            .type(Cypress.env("username"))
            .should('have.value', Cypress.env("username"))
  
          cy.get('#txtPassword')
            .type(Cypress.env("password"))
            .should('have.value', Cypress.env("password"))
  
            cy.get('#btnLogin').click()
  
            //verify Dashboard is visible 
            cy.get('h1').should('have.text', 'Dashboard');
            
            //verify navigation is visible 
            cy.get('#menu_admin_viewAdminModule')
            .should('be.visible')
            .click()

            cy.get('#btnDelete').should('be.visible')
            cy.get('#btnAdd').should('be.visible').click()
            
            cy.get('#systemUser_userType')
            .select('ESS')
            .should('have.value', '2')

            cy.get('#systemUser_employeeName_empName')
            .type('J')
            cy.get('div.ac_results>ul >li')
            .each(function($el){
              // $el is a wrapped jQuery element
              if ($el.text() === 'John Smith' ) {
                // wrap this element so we can
                
                cy.wrap($el).trigger('mouseover').click()
              }
            })

            cy.get('#systemUser_userName')
            .invoke('val', 'MilanTest11')
            .trigger('change')


            cy.get('#systemUser_status')
            .select('Enabled')
            .should('have.value', '1')

            cy.get('#btnSave').click()
            
            
            cy.get('.message')
            .should('be.visible')
            .and('contain', 'Successfully Saved');
             
                

  
          })
        })
    })